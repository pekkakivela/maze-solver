package fi.pekkakivela.mazesolver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class MazeTest {

    @Test
    void test_constructor_validInput() throws Exception {
        String inputString = "#######E########E####################\n" +
                "# ### #   ###### #    #     #     # E\n" +
                "# ### ### #      #  #    #     #    #\n" +
                "# ### # # # ###### ##################\n" +
                "#            #       #    #   #   # #\n" +
                "#  # ##      # ##### #  # # # # # # #\n" +
                "#  #         #   #   #  # # # # #   #\n" +
                "#  ######   ###  #  ### # # # # ### #\n" +
                "#  #    #               #   #   #   #\n" +
                "#  # ## ########   ## ###########   #\n" +
                "#    ##          ###                #\n" +
                "# ## #############  ###   ####   ## #\n" +
                "#  ### ##         #  #  #           #\n" +
                "#  #   ## ####     #    #      ###  #\n" +
                "#  # #### #  #     #    #####       #\n" +
                "#  #      #      ###           ##   #\n" +
                "#  #####           #   ##   #   #   #\n" +
                "#                                   #\n" +
                "##################^##################\n";

        Maze maze = new Maze(inputString);

        Assertions.assertEquals(inputString, maze.getOriginalMaze());
    }

    @Test
    void test_constructor_invalidInput() throws Exception {
        String inputString = "#######E########E####################\n" +
                "# ### #   ###### #    #     #     # E\n" +
                "# ### ### #      #  #    #     #    #\n" +
                "# ### # # # ###### ##################\n" +
                "#            #  123  #    #   #   # #\n" +
                "#  # ##      # ##### #  # # # # # # #\n" +
                "#  #         #   #   #  # # # # #   #\n" +
                "#  ######   ###  #  ### # # # # ### #\n" +
                "#  #    #               #   #   #   #\n" +
                "#  # ## ########   ## ###########   #\n" +
                "#    ##          ###                #\n" +
                "# ## #############  ###   ####   ## #\n" +
                "#  ### ##         #  #  #           #\n" +
                "#  #   ## ####     #    #      ###  #\n" +
                "#  # #### #  #     #    #####       #\n" +
                "#  #      #      ###           ##   #\n" +
                "#  #####           #   ##   #   #   #\n" +
                "#                                   #\n" +
                "##################^##################\n";

        Assertions.assertThrows(Exception.class, () -> new Maze(inputString));
    }

    @Test
    void test_setSolvedMaze() throws Exception {
        String inputString = "#######E########E####################\n" +
                "# ### #   ###### #    #     #     # E\n" +
                "# ### ### #      #  #    #     #    #\n" +
                "# ### # # # ###### ##################\n" +
                "#            #       #    #   #   # #\n" +
                "#  # ##      # ##### #  # # # # # # #\n" +
                "#  #         #   #   #  # # # # #   #\n" +
                "#  ######   ###  #  ### # # # # ### #\n" +
                "#  #    #               #   #   #   #\n" +
                "#  # ## ########   ## ###########   #\n" +
                "#    ##          ###                #\n" +
                "# ## #############  ###   ####   ## #\n" +
                "#  ### ##         #  #  #           #\n" +
                "#  #   ## ####     #    #      ###  #\n" +
                "#  # #### #  #     #    #####       #\n" +
                "#  #      #      ###           ##   #\n" +
                "#  #####           #   ##   #   #   #\n" +
                "#                                   #\n" +
                "##################^##################\n";

        String solvedMaze = "#######E########E####################\n" +
                "# ### #   ###### #    #     #     # E\n" +
                "# ### ### #      #  #    #     #    #\n" +
                "# ### # # # ###### ##################\n" +
                "#            #       #    #   #   # #\n" +
                "#  # ##      # ##### #  # # # # # # #\n" +
                "#  #         #   #   #  # # # # #   #\n" +
                "#  ######   ###  #  ### # # # # ### #\n" +
                "#  #    #               #   #   #   #\n" +
                "#  # ## ########   ## ###########   #\n" +
                "#    ##          ###                #\n" +
                "# ## #############  ###   ####   ## #\n" +
                "#  ### ##         #  #  #           #\n" +
                "#  #   ## ####     #    #      ###  #\n" +
                "#  # #### #  #     #    #####       #\n" +
                "#  #      #      ###           ##   #\n" +
                "#  #####          X#   ##   #   #   #\n" +
                "#                 X                 #\n" +
                "##################^##################\n";

        Maze maze = new Maze(inputString);

        List<Coordinate> coordinates = new ArrayList<>();
        coordinates.add(new Coordinate(18, 18));
        coordinates.add(new Coordinate(17, 18));
        coordinates.add(new Coordinate(16, 18));

        maze.setSolvedMaze(coordinates);

        Assertions.assertEquals(solvedMaze, maze.getSolvedMaze());
    }

}