package fi.pekkakivela.mazesolver.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Utility class for file handling
 */
public class FileUtil {

    /**
     * Reads input file from given location and returns a string
     * @param fileLocation Location of file
     * @return File content as a string
     * @throws FileNotFoundException
     */
    public String readFile(String fileLocation) throws FileNotFoundException {
        File inputFile = new File(fileLocation);
        String fileAsString = "";
        Scanner input = new Scanner(inputFile);
        while (input.hasNextLine()) {
            fileAsString += input.nextLine() + "\n";
        }
        return fileAsString;
    }
}
