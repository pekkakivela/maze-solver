package fi.pekkakivela.mazesolver.exception;

public class MazeSolverException  extends RuntimeException {

    public MazeSolverException(String message, Throwable cause) {
        super(message, cause);
    }

    public MazeSolverException(String message) {
        super(message);
    }
}
