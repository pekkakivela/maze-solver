package fi.pekkakivela.mazesolver;

import fi.pekkakivela.mazesolver.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Main handler that utilizes necessary helper classes for solving the maze.
 */
public class MazeHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    FileUtil fileUtil;
    PathSolver pathSolver;

    public MazeHandler() {
        fileUtil = new FileUtil();
        pathSolver = new PathSolver();
    }

    /**
     * Takes input parameters given by user and tries to solve maze using helper classes.
     * @param inputFileLocation Location of file that contains the maze
     * @param maxMoveAmount Maximum amount of moves to be used
     */
    public void handleMaze(String inputFileLocation, int maxMoveAmount) {
         try {
             logger.info("Attempting to solve maze from {} with maximum move amount of {}", inputFileLocation, maxMoveAmount);

             String mazeAsString = fileUtil.readFile(inputFileLocation);

             Maze maze = new Maze(mazeAsString);
             logger.info("Maze: \n" + maze.getOriginalMaze());

             List<Coordinate> solvedMazeCoordinates = pathSolver.solve(maze, maxMoveAmount);

             maze.setSolvedMaze(solvedMazeCoordinates);

             logger.info("Solved maze: \n" + maze.getSolvedMaze());
         } catch (Exception e) {
            logger.error("Maze solving failed: " + e);
        }
    }
}
