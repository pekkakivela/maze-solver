package fi.pekkakivela.mazesolver;

import fi.pekkakivela.mazesolver.exception.MazeSolverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Maze solver class.
 */
public class PathSolver {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private static final int[][] DIRECTIONS = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
    private int usedMoveAmount = 0;
    private int maxMoveAmount;

    /**
     * Tries to solve given maze using Depth-first search algorithm.
     * @param maze Maze to be solved
     * @param maxMoveAmount Maximum amount of moves that can be used
     * @return Solution coordinates
     * @throws Exception
     */
    public List<Coordinate> solve(Maze maze, int maxMoveAmount) throws Exception {
        this.maxMoveAmount = maxMoveAmount;
        List<Coordinate> path = new ArrayList<>();

        if (explore(maze, maze.getStart().getX(), maze.getStart().getY(), path)) {
            return path;
        }

        return Collections.emptyList();
    }

    private boolean explore(Maze maze, int row, int column, List<Coordinate> path) throws Exception {
        if (!maze.isValidLocation(row, column) || maze.isBlock(row, column) || maze.isExplored(row, column)) {
            return false;
        }

        path.add(new Coordinate(row, column));
        maze.setVisitedCoordinate(row, column, true);

        if (maze.isEnd(row, column)) {
            return true;
        }

        for (int[] direction : DIRECTIONS) {
            Coordinate coordinate = getNextCoordinate(row, column, direction[0], direction[1]);
            if (explore(maze, coordinate.getX(), coordinate.getY(), path)) {
                return true;
            }
        }

        usedMoveAmount++;
        if(usedMoveAmount >= maxMoveAmount) {
            throw new MazeSolverException(String.format("Could not solve maze with %d moves!", maxMoveAmount));
        }

        path.remove(path.size() - 1);
        return false;
    }

    private Coordinate getNextCoordinate(int row, int col, int i, int j) {
        return new Coordinate(row + i, col + j);
    }
}
