package fi.pekkakivela.mazesolver;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Class that holds the details of map that needs to be solved.
 */
public class Maze {
    private final CoordinateValue[][] originalMaze;
    private CoordinateValue[][] solvedMaze;
    private final boolean[][] visitedCoordinates;
    private Coordinate start;
    private Coordinate end;
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final Map<Character, CoordinateValue> COORDINATE_VALUES_MAP = ImmutableMap.<Character, CoordinateValue> builder()
            .put('#', CoordinateValue.Block)
            .put('^', CoordinateValue.StartingPosition)
            .put('E', CoordinateValue.Exit)
            .put(' ', CoordinateValue.Space)
            .put('X', CoordinateValue.Path)
            .build();

    public enum CoordinateValue {
        Block,
        Space,
        Exit,
        StartingPosition,
        Path
    }

    /**
     * Constructor.
     * @param mazeAsString Maze that was given as input to the application in string format.
     */
    public Maze(String mazeAsString) {
        String[] lines = mazeAsString.split("[\r]?\n");

        originalMaze = new CoordinateValue[lines.length][lines[0].length()];
        visitedCoordinates = new boolean[lines.length][lines[0].length()];

        generateMaze(lines);
    }

    /**
     * @return Height of the maze
     */
    public int getHeight() { return originalMaze.length; }

    /**
     * @return Width of the maze
     */
    public int getWidth() {
        return originalMaze[0].length;
    }

    /**
     * @return Starting point of the maze
     */
    public Coordinate getStart() {
        return start;
    }

    /**
     * @param x
     * @param y
     * @return Whether given coordinates were the end of the maze or not.
     */
    public boolean isEnd(int x, int y) {
        return x == end.getX() && y == end.getY();
    }

    /**
     * @param x
     * @param y
     * @return Whether given coordinates were the start of the maze or not.
     */
    public boolean isStart(int x, int y) {
        return x == start.getX() && y == start.getY();
    }

    /**
     * @param row
     * @param column
     * @return Whether given coordinates were already explored.
     */
    public boolean isExplored(int row, int column) {
        return visitedCoordinates[row][column];
    }

    /**
     * @param row
     * @param column
     * @return Whether given coordinates contain a block.
     */
    public boolean isBlock(int row, int column) {
        return originalMaze[row][column] == CoordinateValue.Block;
    }

    /**
     * Sets given coordinates as visited.
     * @param row
     * @param column
     * @param value
     */
    public void setVisitedCoordinate(int row, int column, boolean value) {
        visitedCoordinates[row][column] = value;
    }

    /**
     * Sets value to given coordinate
     * @param row
     * @param column
     * @param value
     */
    public void setCoordinateValue(int row, int column, CoordinateValue value) {
        originalMaze[row][column] = value;
    }

    /**
     * Checks if given coordinates contain a valid location.
     * @param row
     * @param column
     * @return
     */
    public boolean isValidLocation(int row, int column) {
        return row >= 0 && row < getHeight() && column >= 0 && column < getWidth();
    }

    /**
     * Sets solution to the maze.
     * @param solutionPath
     */
    public void setSolvedMaze(List<Coordinate> solutionPath) {
        solvedMaze = Arrays.stream(originalMaze)
                .map(CoordinateValue[]::clone)
                .toArray(CoordinateValue[][]::new);
        for (Coordinate coordinate : solutionPath) {
            if (isStart(coordinate.getX(), coordinate.getY()) || isEnd(coordinate.getX(), coordinate.getY())) {
                continue;
            }
            solvedMaze[coordinate.getX()][coordinate.getY()] = CoordinateValue.Path;
        }
    }

    /**
     * @return Original maze as string.
     */
    public String getOriginalMaze() {
        return getMazeAsString(originalMaze);
    }

    /**
     * @return Solved maze as string.
     */
    public String getSolvedMaze() {
        return getMazeAsString(solvedMaze);
    }

    private void generateMaze(String[] lines) {
        for (int row = 0; row < getHeight(); row++) {
            for (int column = 0; column < getWidth(); column++) {
                CoordinateValue coordinateValue = COORDINATE_VALUES_MAP.getOrDefault(lines[row].charAt(column), null);
                if(coordinateValue == null) {
                    throw new IllegalArgumentException("Invalid value in input map!");
                }

                setCoordinateValue(row, column, coordinateValue);
                if(coordinateValue == CoordinateValue.StartingPosition) {
                    start = new Coordinate(row, column);
                } else if(coordinateValue == CoordinateValue.Exit) {
                    end = new Coordinate(row, column);
                }
            }
        }
    }

    private String getMazeAsString(CoordinateValue[][] maze) {
        StringBuilder result = new StringBuilder(getWidth() * (getHeight() + 1));
        for (int row = 0; row < getHeight(); row++) {
            for (int column = 0; column < getWidth(); column++) {
                CoordinateValue coordinateValue = maze[row][column];
                Character characterValue = COORDINATE_VALUES_MAP.keySet()
                        .stream()
                        .filter(key -> coordinateValue == COORDINATE_VALUES_MAP.get(key))
                        .findFirst().get();
                result.append(characterValue);
            }
            result.append('\n');
        }
        return result.toString();
    }
}
