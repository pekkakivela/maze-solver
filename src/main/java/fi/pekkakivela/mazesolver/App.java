package fi.pekkakivela.mazesolver;

/**
 * Main class of Maze Solver application.
 */
public class App {

    public static void main(String[] args) {
        String inputFileLocation;
        int maxMoveAmount;

        try{
            inputFileLocation = args[0];
            maxMoveAmount = Integer.parseInt(args[1]);
        }
        catch (Exception e){
            throw new IllegalArgumentException("Invalid input parameters");
        }

        MazeHandler mazeHandler = new MazeHandler();
        mazeHandler.handleMaze(inputFileLocation, maxMoveAmount);
    }
}
