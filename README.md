# Maze Solver

Tries to solve maze from file with given maximum move amount

Clean build:

	mvn clean install

Run:
	
    java -jar target\maze-solver-1.0.jar [InputFileLocation] [MaxMoveAmount] -> e.g. java -jar target\maze-solver-1.0.jar src/test/resources/maze-task-first.txt 200